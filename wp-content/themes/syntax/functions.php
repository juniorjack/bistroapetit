<?php
/**
 * Readable functions and definitions
 *
 * @package Syntax
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 1008; /* pixels */

if ( ! function_exists( 'syntax_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function syntax_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Readable, use a find and replace
	 * to change 'syntax' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'syntax', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( '1008', '9999' );

	/*
	 * Add support for TinyMCE styles
	 */
	add_editor_style();

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary'   => __( 'Primary Menu', 'syntax' ),
		'secondary' => __( 'Footer Menu', 'syntax' ),
	) );

	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'syntax_custom_background_args', $args );

	add_theme_support( 'custom-background', $args );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', 'gallery' ) );
}
endif; // syntax_setup
add_action( 'after_setup_theme', 'syntax_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function syntax_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 1', 'syntax' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 2', 'syntax' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 3', 'syntax' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'syntax_widgets_init' );

/**
 * Enqueue scripts and styles
 */

function website_scripts() {
	if( is_page( 'homepage' ) ){
		//first we must register the responsive framework's scripts
	    wp_register_script('foundation-modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', array( 'jquery' ), false ); 
	    wp_register_script('foundation-main', get_template_directory_uri() . '/js/foundation.min.js', true );
		wp_register_script('main.js', get_template_directory_uri() . '/js/main.js', true );
		wp_register_script('scrolld.min.js', get_template_directory_uri() . '/js/scrolld.min.js', true );
 
	    wp_enqueue_script( 'foundation-modernizr' ); 
	    wp_enqueue_script( 'foundation-main' );
	    wp_enqueue_script( 'main.js' );
	    wp_enqueue_script( 'scrolld.min.js' );

	    //register styles for our theme 
	    wp_register_style( 'homepage-stylesheet', get_template_directory_uri() . '/css/style-homepage.css', array(), 'all' );
	    wp_register_style( 'foundation-stylesheet', get_template_directory_uri() . '/css/foundation.css', array(), 'all' );
	    wp_register_style( 'foundation-normalize', get_template_directory_uri() . '/css/normalize.css', array(), 'all' );
	    wp_register_style( 'fonts-stylesheet', get_template_directory_uri() . '/css/fonts.css', array(), 'all' );

	    wp_enqueue_style( 'homepage-stylesheet' );
	    wp_enqueue_style( 'foundation-stylesheet' );
	    wp_enqueue_style( 'foundation-normalize' );
	    wp_enqueue_style( 'fonts-stylesheet' );
	} else {
		wp_enqueue_style( 'syntax-style', get_stylesheet_uri() );
		wp_enqueue_style( 'syntax-merriweather' );
		wp_enqueue_script( 'syntax-siteheader', get_template_directory_uri() . '/js/siteheader.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'syntax-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		if ( is_singular() && wp_attachment_is_image() ) {
			wp_enqueue_script( 'syntax-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'website_scripts' );


/**
 * Register Google Fonts
 */
function syntax_google_fonts() {

	$protocol = is_ssl() ? 'https' : 'http';

	/*	translators: If there are characters in your language that are not supported
		by Merriweather, translate this to 'off'. Do not translate into your own language. */

	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'syntax' ) ) {

		wp_register_style( 'syntax-merriweather', "$protocol://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" );

	}

}
add_action( 'init', 'syntax_google_fonts' );

/**
 * Enqueue Google Fonts for custom headers
 */
function syntax_admin_scripts( $hook_suffix ) {

	if ( 'appearance_page_custom-header' != $hook_suffix )
		return;

	wp_enqueue_style( 'syntax-merriweather' );

}
add_action( 'admin_enqueue_scripts', 'syntax_admin_scripts' );

/**
 * Adds additional stylesheets to the TinyMCE editor if needed.
 *
 * @param string $mce_css CSS path to load in TinyMCE.
 * @return string
 */
function syntax_mce_css( $mce_css ) {

	$protocol = is_ssl() ? 'https' : 'http';

	$font = "$protocol://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic";

	if ( empty( $font ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$font= esc_url_raw( str_replace( ',', '%2C', $font ) );

	return $mce_css . $font;
}
add_filter( 'mce_css', 'syntax_mce_css' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( file_exists( get_template_directory() . '/inc/jetpack.php' ) )
	require get_template_directory() . '/inc/jetpack.php';

// Equip costum post types
require get_template_directory() . '/inc/cuztom.php';

// Custum functions
// Featured image url
function FeaturedImageSrc($PostId){
	$slika = get_the_post_thumbnail($PostId);
	$slike = explode('"',$slika);
	$url = $slike[5];
	echo $url;
}
// Function for switching post if depending on language
function PostLanguageId($PostIdEN, $PostIdHR){
	$language = $_GET['lang'];
	if($language!='hr'){
		return $PostIdEN;
	}
	else{
		return $PostIdHR;
	}
}
// Function that displays links for translate site
function TranslationLinks(){
	$language = $_GET['lang'];
	if($language=='hr'){
		echo '<a href="'.get_site_url().'">EN</a>';
	}
	else{
		echo '<a href="'.get_site_url().'/?lang=hr">HR</a>';	
	}
}

// Function that displays links for translate site
function BlogLangLinks(){
	$language = $_GET['lang'];
	if($language=='hr'){
		echo '<a href="'.get_site_url().'/blog/?lang=hr">blog</a>';
	}
	else{
		echo '<a href="'.get_site_url().'/blog">blog</a>';	
	}
}


// Menu on the left: adjustment for margin left
function MarginLeftMeniLinkHR(){
	$language = $_GET['lang'];
	if($language=='hr'){
		echo '<style type="text/css">#content-nav nav ul li a{margin-left: 16px !important;}</style>';
	}	
}

// Admin panel: Menu adjustments
function edit_admin_menus() {
    remove_menu_page('edit-comments.php');
}
add_action( 'admin_menu', 'edit_admin_menus' );

function ap_remove_jetpack_page( ) {
	if ( class_exists( 'Jetpack' )) {
		remove_menu_page( 'jetpack' );
	}
}
add_action( 'admin_menu', 'ap_remove_jetpack_page', 999 );

function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
     
    return array(
        'index.php', // Dashboard
        'separator1', // First separator
        'edit.php', // Posts
        'edit.php?post_type=page', // Pages
        'edit.php?post_type=menu', // Menu
        'edit.php?post_type=drink', // Drink
        'edit.php?post_type=special_days', // Special days
        'separator2', // Second separator
        'upload.php', // Media
        'themes.php', // Appearance
        'plugins.php', // Plugins
        'users.php', // Users
        'tools.php', // Tools
        'options-general.php', // Settings
        'separator-last', // Last separator
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');