		<footer id="contact">
			<div class="footer-content">
				<div class="footer-container">
					<?php
					$Contact = get_post(PostLanguageId(96, 109), ARRAY_A);
					echo wpautop($Contact['post_content']);	
					?>

					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/maestro.jpg">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/mastercard.jpg">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/american.jpg">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/diners.jpg">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/visa.jpg">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cards/discover.jpg">
					<a href="https://www.facebook.com/bistroapetitzagreb" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/images/social/fb.png"><span>Bistro Apetit Facebook</span></a>
				</div><!-- footer container -->
			</div><!-- footer content -->
		</footer>

		<?php wp_footer(); ?>
		<script type="text/javascript">
			var anim_B_F="foward";
			jQuery("#content-nav").click(function(){
			  if(anim_B_F=="foward" && meni == 'ON')
			  {
			    jQuery('#content-nav').stop(true, false).animate({ marginLeft: "0" });
			    anim_B_F="back";
			  }
			});

			jQuery("#content-nav").mouseleave(function(){
			  if(anim_B_F=="back" && meni == 'ON')
			  {
			    jQuery('#content-nav').stop(true, false).animate({ marginLeft: "-346" });
			    anim_B_F="foward";
			  }
			});
		</script>
		
		<script type="text/javascript">
			jQuery("[id*='Btn']").stop(true).on('click', function (e) {e.preventDefault();jQuery(this).scrolld();})
		</script>
		
		<script type="text/javascript">
			( function( $ ) {
				$( document ).foundation();
			} ) ( jQuery )
		</script>
	</body>
</html>