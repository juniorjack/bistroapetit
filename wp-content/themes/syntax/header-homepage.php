<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<?php MarginLeftMeniLinkHR(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
		<div class="logo-container fade" style="display: none;">
			<a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Bistro Apetit"><h1>Bistro Apetit</h1></a>
		</div><!-- logo container -->

		<nav class="fade" style="display: none;">
			<ul>
				<li><a id="our-storyBtn" href="#our-story"><?php _e( 'our story', 'bistroapetit' ) ?></a></li>
				<li><a id="menuBtn" href="#"><?php _e( 'menu', 'bistroapetit' ) ?></a></li>
				<li><a id="winesBtn" href="#"><?php _e( 'wines', 'bistroapetit' ) ?></a></li>
				<li><a id="contactBtn" href="#"><?php _e( 'contact', 'bistroapetit' ) ?></a></li>
				<li><?php BlogLangLinks(); ?></li>
				<li><?php TranslationLinks(); ?></li>
			</ul>
		</nav>
	</header>