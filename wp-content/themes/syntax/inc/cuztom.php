<?php

define( 'CUZTOM_TEXTDOMAIN', 'wittebergen' );
define( 'CUZTOM_URL', get_template_directory_uri() . '/inc/cuztom' );

require( 'cuztom/cuztom.php' );

$Menu = new Cuztom_Post_Type( 'Menu', array(
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'show_column' => true
) );

$drink = new Cuztom_Post_Type( 'drink', array(
	'supports' => array( 'title' ),
	'show_column' => true
) );

$drink->add_meta_box( 
	'sadrzaj',
	'Sadrzaj', 
	array(
		array(
			'name' 			=> 'lijevo',
			'label' 		=> 'Lijevo',
			'description'	=> 'SadrŽaj u lijevom stupcu',
			'type'			=> 'wysiwyg'
		),
		array(
			'name' 			=> 'desno',
			'label' 		=> 'Desno',
			'description'	=> 'SadrŽaj u desnom stupcu',
			'type'			=> 'wysiwyg'
		)
	)
);

$special_days = new Cuztom_Post_Type( 'special_days', array(
	'supports' => array( 'title', 'editor', 'thumbnail' ),
	'show_column' => true
) );