<?php
/*
 Template Name: Homepage 
*/

get_header('homepage'); ?>

<div class="header-bottom">
	<img src="<?php echo get_template_directory_uri(); ?>/images/header-bottom.png">
</div><!-- header bottom -->

<div class="our-story" id="our-story">
	<div id="content-nav">
		<nav>
			<ul>
				<li><a id="our-storyBtn" href="#our-story"><?php _e( 'our story', 'bistroapetit' ) ?></a></li>
				<li><a id="menuBtn" href="#"><?php _e( 'menu', 'bistroapetit' ) ?></a></li>
				<li><a id="winesBtn" href="#"><?php _e( 'wines', 'bistroapetit' ) ?></a></li>
				<li><a id="contactBtn" href="#"><?php _e( 'contact', 'bistroapetit' ) ?></a></li>
				
				<li><?php BlogLangLinks(); ?></li>
				<li><?php TranslationLinks(); ?></li>
				<li><img src="<?php echo get_template_directory_uri(); ?>/images/menu-logo.png"></li>
			</ul>
		</nav>
	</div><!-- content nav -->

	<div class="our-story-content">
		<div class="our-story-container">
			<?php 
			$our_story = get_post(PostLanguageId(89, 107), ARRAY_A); 
			echo wpautop($our_story['post_content']);	
			?>
		</div><!-- our story container -->
	</div><!-- our story inner -->
</div><!-- our story -->

<div class="menu" id="menu">
	<div class="menu-single-left first-menu">
		<div class="menu-single-container">
			<h1>Menu</h1>
			<?php 
			$starters = get_post(PostLanguageId(86, 98), ARRAY_A); 
			echo '<h1>'.$starters['post_title'].'</h1>';
			echo wpautop($starters['post_content']);	
			?>
		</div><!-- menu single container -->

		<div class="featured-image" style="background-image:url(<?php FeaturedImageSrc(86); ?>);">
		</div><!-- featured image -->
	</div><!-- menu single -->

	<div class="menu-single-right">
		<div class="menu-single-container">
			<?php 
			$intermezzo = get_post(PostLanguageId(92, 99), ARRAY_A); 
			echo '<h1>'.$intermezzo['post_title'].'</h1>';
			echo wpautop($intermezzo['post_content']);	
			?>	

			<?php 
			$soups = get_post(PostLanguageId(93, 100), ARRAY_A); 
			echo '<h1>'.$soups['post_title'].'</h1>';
			echo wpautop($soups['post_content']);	
			?>
		</div><!-- menu single container -->
		<div class="featured-image" style="background-image:url(<?php FeaturedImageSrc(92); ?>);">
		</div><!-- featured image -->
	</div><!-- menu single -->

	<div class="menu-single-left">
		<div class="menu-single-container">
			<?php 
			$TheMostBeautifulMoment = get_post(PostLanguageId(94, 101), ARRAY_A); 
			echo '<h1>'.$TheMostBeautifulMoment['post_title'].'</h1>';
			echo wpautop($TheMostBeautifulMoment['post_content']);	
			?>
		</div><!-- menu single container -->
		<div class="featured-image" style="background-image:url(<?php FeaturedImageSrc(94); ?>);">
		</div><!-- featured image -->
	</div><!-- menu single -->

	<div class="menu-single-right">
		<div class="menu-single-container">
			<?php
			$SideDishesAndSalads = get_post(PostLanguageId(95, 102), ARRAY_A); 
			echo '<h1>'.$SideDishesAndSalads['post_title'].'</h1>';
			echo wpautop($SideDishesAndSalads['post_content']);	
			?>
		</div><!-- menu single container -->
		<div class="featured-image" style="background-image:url(<?php FeaturedImageSrc(95); ?>);">
		</div><!-- featured image -->
	</div><!-- menu single -->

	<div class="menu-single-left">
		<div class="menu-single-container">
			<?php 
			$SweetMoment = get_post(PostLanguageId(188, 189), ARRAY_A); 
			echo '<h1>'.$SweetMoment['post_title'].'</h1>';
			echo wpautop($SweetMoment['post_content']);	
			?>
		</div><!-- menu single container -->
		<div class="featured-image" style="background-image:url(<?php FeaturedImageSrc(188); ?>);">
		</div><!-- featured image -->
	</div><!-- menu single -->
</div><!-- menu -->

<div class="wines" id="wines">
	<div class="row">
		<div class="large-8 medium-10 columns large-offset-2 medium-offset-1">
			<div class="wines-container">
				<ul class="tabs" data-tab>
					<li class="tab-title active"><a href="#panel2-1"><?php _e( 'champagne', 'bistroapetit' ) ?></a></li>
					<li class="tab-title"><a href="#panel2-2"><?php _e( 'white', 'bistroapetit' ) ?></a></li>
					<li class="tab-title"><a href="#panel2-3"><?php _e( 'rose', 'bistroapetit' ) ?></a></li>
					<li class="tab-title"><a href="#panel2-4"><?php _e( 'red', 'bistroapetit' ) ?></a></li>
					<li class="tab-title"><a href="#panel2-5"><?php _e( 'dessert', 'bistroapetit' ) ?></a></li>
				</ul>

				<div class="tabs-content">
					<div class="content active" id="panel2-1">
						<?php
						$Champagne = get_post(PostLanguageId(73, 115), ARRAY_A); 
						echo '<h1>'.$Champagne['post_title'].'</h1>';	
						?>
						<div class="tabs-content-left">
						<?php echo get_post_meta(PostLanguageId(73, 115), '_sadrzaj_lijevo', true ); ?>
						</div><!-- tabs content left -->

						<div class="tabs-content-right">
						<?php echo get_post_meta(PostLanguageId(73, 115), '_sadrzaj_desno', true ); ?>	
						</div><!-- tabs content right -->
					</div>

					<div class="content" id="panel2-2">
						<?php
						$White = get_post(PostLanguageId(74, 114), ARRAY_A); 
						echo '<h1>'.$White['post_title'].'</h1>';	
						?>
						<div class="tabs-content-left">
						<?php echo get_post_meta( PostLanguageId(74, 114), '_sadrzaj_lijevo', true ); ?>
						</div><!-- tabs content left -->

						<div class="tabs-content-right">
						<?php echo get_post_meta( PostLanguageId(74, 114), '_sadrzaj_desno', true ); ?>	
						</div><!-- tabs content right -->
					</div>

					<div class="content" id="panel2-3">
						<?php
						$Rose = get_post(PostLanguageId(75, 116), ARRAY_A); 
						echo '<h1>'.$Rose['post_title'].'</h1>';	
						?>
						<div class="tabs-content-left">
						<?php echo get_post_meta( PostLanguageId(75, 116), '_sadrzaj_lijevo', true ); ?>
						</div><!-- tabs content left -->

						<div class="tabs-content-right">
						<?php echo get_post_meta( PostLanguageId(75, 116), '_sadrzaj_desno', true ); ?>	
						</div><!-- tabs content right -->
					</div>

					<div class="content" id="panel2-4">
						<?php
						$Red = get_post(PostLanguageId(76, 113), ARRAY_A); 
						echo '<h1>'.$Red['post_title'].'</h1>';	
						?>
						<div class="tabs-content-left">
						<?php echo get_post_meta( PostLanguageId(76, 113), '_sadrzaj_lijevo', true ); ?>
						</div><!-- tabs content left -->

						<div class="tabs-content-right">
						<?php echo get_post_meta( PostLanguageId(76, 113), '_sadrzaj_desno', true ); ?>	
						</div><!-- tabs content right -->
					</div>

					<div class="content" id="panel2-5">
						<?php
						$Dessert = get_post(PostLanguageId(167, 166), ARRAY_A); 
						echo '<h1>'.$Dessert['post_title'].'</h1>';	
						?>
						<div class="tabs-content-left">
						<?php echo get_post_meta( PostLanguageId(167, 166), '_sadrzaj_lijevo', true ); ?>
						</div><!-- tabs content left -->

						<div class="tabs-content-right">
						<?php echo get_post_meta( PostLanguageId(167, 166), '_sadrzaj_desno', true ); ?>	
						</div><!-- tabs content right -->
					</div>

				</div><!-- tabs content -->
			</div><!-- wines container -->
		</div><!-- columns -->
	</div><!-- row -->
</div><!-- wines -->

<div class="special-days">
	<div class="tabs-menu">
		<h2>/Special Days</h2>

		<ul class="tabs" data-tab>
		<?php $loop = new WP_Query( array( 'post_type' => 'special_days', 'posts_per_page' => 4 ) ); ?>
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php if ($loop->current_post == 0): ?>
				<li class="tab-title active"><a href="#panel<?php echo get_the_ID(); ?>"><?php the_title(); ?></a></li>
			<?php else: ?>
				<li class="tab-title"><a href="#panel<?php echo get_the_ID(); ?>"><?php the_title(); ?></a></li>
			<?php endif ?>
		<?php endwhile; ?>
		</ul><!-- /.tabs -->
	</div><!-- /.left -->
	
	<div class="content-container">
		<div class="tabs-content">
			<?php $loop = new WP_Query( array( 'post_type' => 'special_days', 'posts_per_page' => 4 ) ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

			<?php if ($loop->current_post == 0): ?>
				<div class="content active" id="panel<?php echo get_the_ID(); ?>">
			<?php else: ?>
				<div class="content" id="panel<?php echo get_the_ID(); ?>">
			<?php endif ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<div class="bg-image" style="background-image: url(<?php echo $url; ?>);"></div><!-- /.bg-image -->

				<?php the_content(); ?>
			</div><!-- /.content -->

			<?php endwhile; ?>
		</div><!-- /.tabs-content -->
	</div><!-- /.right -->
</div><!-- /.special-days -->

<?php get_footer('homepage'); ?>