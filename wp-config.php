<?php
//The entry below were created by iThemes Security to disable the file editor
define( 'DISALLOW_FILE_EDIT', true );
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bistroapetit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jM*Yit<6GU_@5#p9u&uVy@B`[2aM&cjI;RLxze3U@b+o-3#+gSiU}Xi1>#_ybNk#');
define('SECURE_AUTH_KEY',  '8yRu-dLFzqH@+e5|XKg,a+?Vrs`Dyx-3--eE n[5jU`@d&Ju+?=Y.v&hk|[^zw87');
define('LOGGED_IN_KEY',    'C-;|12^S.%WyAknsN_[M,wrS>Gs{:F?GnZ&,iEOmmT4eb&?]L2,1E&=AhUuf(B,y');
define('NONCE_KEY',        'J=3=K7t|]&:lTG-wnI&?N&jpOTF5RP2X091c++GHG_)-NYfiUbr`Ss_fLYMR71##');
define('AUTH_SALT',        'aijrCma4J:4U@DAOc]ViUI){{aT9d8o-ZG{G?nisyH>g-,V-We0V/4pJHO;0yP]R');
define('SECURE_AUTH_SALT', 'Fv,{V|lq@;Jn!=?*+e v4jJce*L}N0@_nQ,E,-s=M|&OlHdNu+u<JUA{{4?DX%q7');
define('LOGGED_IN_SALT',   '6x80[30:oCvK%r4cAHV}L?=r6T.|-Iqpu7lGZ3{LE&*-crv&^fI[5R+mRX-mln#a');
define('NONCE_SALT',       'P9V<Po|2P7UNt)|XM3Ry)s41Ca5n,=dJPoWHY*=01VhIpC}RBk9s-4oxitu$(Yj=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'kqzxhs4h9_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
